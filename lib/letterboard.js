'use strict';

const Direction = require('./constants').Direction;
const Move = require('./types').Move;

function getIndex(arr, val) {
    let closer = Infinity
    let min = Infinity
    for(let i = 0; i < arr.length; i++)
        if (arr[i] === val) {
            let tempMin = Math.min(i, arr.length - i)
            min = Math.min(min, tempMin)
            closer = min === tempMin ? i : closer
        }

    return closer
}

module.exports = {
    /*
     * For the board given (an array of strings each representing a letter) and a
     * desired word (a string) returns an array of Move instances.
     */
    // This is a terrible solution, but I ran out of time while trying to implement a more efficient one

    solveLetterBoard: function(board, word) {
        // TODO: write your solution here.  Do not change the method signature in any way, or validation of your solution will fail.
        const answer = []
        // cloning the board to reduce side-effects
        let boardClone = [...board]
        for (let char of word) {
            // Getting the index of one of the elements closer to the edges of the array
            const index = getIndex(boardClone, char)
            // Checking if this is closer to the beginning or the end of the array
            const isCloserToLeft = index < (boardClone.length / 2)

            // Getting the index, the method
            const indexForSearch = isCloserToLeft ? 0 : boardClone.length - 1
            const removalMethod = isCloserToLeft ? 'shift' : 'pop'
            const move = isCloserToLeft ? Direction.LEFT : Direction.RIGHT

            // Changing the array until we find the right element
            while (char !== boardClone[indexForSearch]) {
                const removed = boardClone[removalMethod]()
                boardClone = isCloserToLeft ? [...boardClone, removed] : [removed, ...boardClone]
                answer.push(Move(move))
            }

            // Finally removing the character we found
            boardClone[removalMethod]()
            answer.push(Move(move, char))
        }

        return answer
    }
}